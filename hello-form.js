
var http = require('http'),
    url = require('url'),
    path = require("path"),
    fs = require("fs"),
    querystring = require('querystring'),
    port = 8081;

var server = http.createServer(function(request, response) {

    switch(request.url) {
        case '/':
        case '/index.html':
            fs.readFile('./hello-form.html', function(error, content) {
                response.writeHead(200, { 'Content-Type': 'text/html' });
                response.end(content, 'utf-8');
            });
            break;
        case '/hello':
            var name = 'Stranger',
                queryData = '';
            if(request.method == 'POST') {
                request.on('data', function (data) {
                    queryData += data;
                    if (queryData.length > 1e6) {
                        queryData = "";
                        response.writeHead(413, {'Content-Type': 'text/plain'}).end();
                        request.connection.destroy();
                    }
                });
                request.on('end', function () {
                    request.post = querystring.parse(queryData);
                    if (!!request.post.name) {
                        name = request.post.name;
                    }
                    response.writeHead({'Content-Type': 'text/html'});
                    response.end('Hello, ' + name + '!');
                });
            } else {
                response.writeHead({'Content-Type': 'text/html'});
                response.end('Hello, ' + name + '!');
            }


            break;
        default:
            response.writeHead(404, {'Content-Type':'text/html'});
            response.end('Not found.');
    }


}).listen(port);