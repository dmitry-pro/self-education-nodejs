
var http = require('http'),
    url = require('url'),
    port = 8081;

var server = http.createServer(function(request, response) {
    response.writeHead({'Content-Type':'text/html'});

    var name = 'Stranger',
        parsedQuery = url.parse(request.url, true).query;

    switch(request.url) {
        case '/hello':
            if (!!parsedQuery.name) {
                name = parsedQuery.name;
            }
            response.end('Hello, ' + name + '!');
            break;
        case '/bye':
            if (!!parsedQuery.name) {
                name = parsedQuery.name;
            }
            response.end('Bye, ' + name + '!');
            break;
        default:
            response.end('This is the default route');
    }

}).listen(port);
