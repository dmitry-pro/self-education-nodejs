
var http = require('http'),
    port = 8081;

var server = http.createServer(function(request, response) {
    response.writeHead(200, {'Content-Type': 'text/html'});
    switch(request.url) {
        case '/hello':
            response.end('Hello there!');
            break;
        case '/bye':
            response.end('Goodbye, blue sky!');
            break;
        default:
            response.end('This is the default route');
    }
}).listen(port);
