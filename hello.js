
var http = require('http'),
    port = 8081;

http.createServer(function(request, response) {
    response.writeHead(200, {'Content-Type': 'text/html'});
    response.end('Hello, world!')
}).listen(port);